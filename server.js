const express = require('express');
const mongoose = require('mongoose');
const product = require('./modalSchema');
const app = express();

app.use(express.json());

//get all list
app.get('/get', async(req,res) => {
    try {
        const Product = await product.find({});
        res.status(200).json(Product); 
    } catch (error) {
        res.status(400).json({message: error.message})
    }
})


//post new data
app.post('/post',async(req,res) => {
    try {
        const Product = await product.create(req.body);
        res.status(200).send({message:"new data posted successfully"})
    } catch (error) {
        res.status(400).send({message: error.message})
    }
})

//update existing
app.put('/update/:id',async(req,res) => {
    try {
        const {id} = req.params;
        const Product = await product.findByIdAndUpdate(id,req.body);

        if(!product){
            return res.status(404).json({message:'cannot find any product with ID'})
        }
        res.status(200).send({message:"Data is updated on id:"+id});
    } catch(error) {
        res.status(400).send({message: error.message})
    }
})

//delete post
app.delete('/delete/:id',async(req,res) => {
    try {
        const {id} = req.params;
        const Product = await product.findByIdAndDelete(id);

        if(!product){
            return res.status(404).json({message:'cannot find any product with ID'})
        }
        res.status(200).send({message:"Data is delete on id:"+id});
    } catch(error) {
        res.status(400).send({message: error.message})
    }
})


app.listen(3000, ()=>{
    console.log('testing http://localhost:3000');
})

mongoose.set('strictQuery',false);
mongoose.
connect('mongodb+srv://manickam29500:ySudzlPQiWFaBMnk@cluster0.pvjabdx.mongodb.net/?retryWrites=true&w=majority')
.then(() => {
    console.log('MongoDB is connected');
}).catch((e) => {
    console.log('Error : '+e);
});