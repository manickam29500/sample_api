const mongoose = require('mongoose');

const dbSchema = mongoose.Schema(
    {
        username: {
            type:String,
            required: [true,'this field is required']
        },
        email: {
            type:String,
            required: [true,'this field is required']
        },
        password: {
            type:String,
            required: [true,'this field is required']
        },
    }
)

const schema = mongoose.model('schema',dbSchema);
module.exports = schema;